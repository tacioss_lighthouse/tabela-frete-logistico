const express = require("express");
const cors = require('cors')
const {errors} = require('celebrate');

const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')

const { JWT_SECRET } = require("./config");
const loginRequired = require("./middleware/auth");
const authController = require("./controllers/AuthController");
const usersController = require("./controllers/UsersController");
const logradouroController = require("./controllers/LogradouroController");
const brickController = require("./controllers/BrickController");
const transportadoraController = require("./controllers/TransportadoraController");
const cdsController = require("./controllers/CDsController");

const UsersValidator = require("./validators/UsersValidator");
const AuthValidator = require("./validators/AuthValidator");
const LogradouroValidator = require("./validators/LogradouroValidator");
const BrickValidator = require("./validators/BrickValidator");
const TransportadoraValidator = require("./validators/TransportadoraValidator");
const CDValidator = require("./validators/CDValidator");
const BrickController = require("./controllers/BrickController");

const CDConfigValidator = require('./validators/CDConfigValidator')
const cdConfigController = require('./controllers/CDConfigController')

const queryController = require('./controllers/QueryController')
const trasportadoraConfigFieldsController = require('./controllers/TrasportadoraConfigFieldsController')

const HorariosValidator = require('./validators/HorariosValidator')
const horariosController = require('./controllers/HorarioController')

const tiposTransporteController = require('./controllers/TiposTransporteController')


function createApp() {

  const app = express()
  
  app.use(express.json())
  app.use(cors())
  
  app.route('/users').post(UsersValidator.create, usersController.create)
  app.route('/users/:id').put(loginRequired(JWT_SECRET), UsersValidator.update, usersController.update)

  app.route('/login').post(AuthValidator.login, authController.login)

  app.route('/ceps')
    .get(loginRequired(JWT_SECRET), logradouroController.list)
    .post(loginRequired(JWT_SECRET), LogradouroValidator.create, logradouroController.create)
  app.route('/ceps/search')
    .get(loginRequired(JWT_SECRET), LogradouroValidator.search, logradouroController.search)
  app.route('/ceps/:logradouroId')
    .put(loginRequired(JWT_SECRET), LogradouroValidator.update, logradouroController.update)
    .delete(loginRequired(JWT_SECRET), LogradouroValidator.delete, logradouroController.deleteLogradouro)
    .get(loginRequired(JWT_SECRET), LogradouroValidator.get, logradouroController.get)

  app.route('/bricks')
    .get(loginRequired(JWT_SECRET), brickController.list)
    .post(loginRequired(JWT_SECRET), BrickValidator.create, brickController.create)
  
  app.route('/bricks/:brickId')
    .put(loginRequired(JWT_SECRET), BrickValidator.update, brickController.update)
    .delete(loginRequired(JWT_SECRET), BrickValidator.delete, brickController.deleteBrick)
    .get(loginRequired(JWT_SECRET), BrickValidator.get, BrickController.get)
  
  app.route('/bricks/:brickId/logradouros/:logradouroId')
    .post(loginRequired(JWT_SECRET), BrickValidator.addLogradouro, brickController.addLogradouro)
    .delete(loginRequired(JWT_SECRET), BrickValidator.deleteLogradouro, brickController.deleteLogradouro)
  app.route('/bricks/:brickId/logradouros/')
    .get(loginRequired(JWT_SECRET), BrickValidator.listLogradouros, brickController.listLogradouros)
  

  app.route('/cd-config/:configId/transportadoras/:transportadoraId')
    .post(loginRequired(JWT_SECRET), CDConfigValidator.addTransportadora, cdConfigController.addTransportadora)
    .delete(loginRequired(JWT_SECRET), CDConfigValidator.deleteTransportadora, cdConfigController.deleteTransportadora)
  app.route('/cd-config/:configId/transportadoras/')
    .get(loginRequired(JWT_SECRET), CDConfigValidator.listTransportadoras, cdConfigController.listTransportadoras)

  app.route('/cd-config/:configId/transporte/')
    .get(loginRequired(JWT_SECRET), CDConfigValidator.listTransporte, cdConfigController.listTransporte)
    .post(loginRequired(JWT_SECRET), CDConfigValidator.addTransporte, cdConfigController.addTransporte)

  app.route('/cd-config/transporte/many')
    .get(cdConfigController.listManyTransporte)

  app.route('/cd-config/:configId/transporte/:transporteId')
    .get(loginRequired(JWT_SECRET), CDConfigValidator.showTransporte, cdConfigController.showTransporte)
    .put(loginRequired(JWT_SECRET), CDConfigValidator.updateTransporte, cdConfigController.updateTransporte)

  app.route('/cd-config')
    .post(loginRequired(JWT_SECRET), CDConfigValidator.create, cdConfigController.create)
    .get(loginRequired(JWT_SECRET), cdConfigController.list)
  app.route('/cd-config/:configId')
    .put(loginRequired(JWT_SECRET), CDConfigValidator.update, cdConfigController.update)
    .get(loginRequired(JWT_SECRET), CDConfigValidator.get, cdConfigController.get)
    .delete(loginRequired(JWT_SECRET), CDConfigValidator.delete, cdConfigController.deleteConfig)

  app.route('/transportadora')
    .get(loginRequired(JWT_SECRET), transportadoraController.list)
    .post(loginRequired(JWT_SECRET), TransportadoraValidator.create, transportadoraController.create)
  app.route('/transportadora/:transportadoraId')
    .put(loginRequired(JWT_SECRET), TransportadoraValidator.update, transportadoraController.update)
    .delete(loginRequired(JWT_SECRET), TransportadoraValidator.delete, transportadoraController.deleteTransportadora)  
    .get(loginRequired(JWT_SECRET), TransportadoraValidator.get, transportadoraController.get)  

  app.route('/cds')
    .post(loginRequired(JWT_SECRET), CDValidator.create, cdsController.create)
    .get(loginRequired(JWT_SECRET), cdsController.list)
    
  app.route('/cds')
    .post(loginRequired(JWT_SECRET), CDValidator.create, cdsController.create)
    .get(loginRequired(JWT_SECRET), cdsController.list)
  
  app.route('/cds/:cdId')
    .put(loginRequired(JWT_SECRET), CDValidator.update, cdsController.update)
    .delete(loginRequired(JWT_SECRET), CDValidator.delete, cdsController.deleteCD)
    .get(loginRequired(JWT_SECRET), CDValidator.get, cdsController.get)

  app.route('/cds/:cdId/details')
    .get(CDValidator.get, cdsController.details)

  app.route('/bricks/:brickId/cds/')
    .get(loginRequired(JWT_SECRET), CDValidator.listCdsByBrick, cdsController.listCdsByBrick)
  app.route('/bricks/:brickId/cds/:cdId')
    // TODO: Cirar config padrão
    .post(loginRequired(JWT_SECRET), CDValidator.addBrick, cdsController.addBrick)
    .delete(loginRequired(JWT_SECRET), CDValidator.deleteBrick, cdsController.deleteBrick)

  app.route('/horarios')
    .post(loginRequired(JWT_SECRET), HorariosValidator.create, horariosController.create)
    .get(loginRequired(JWT_SECRET), horariosController.list)
  app.route('/horarios/:id')
    .put(loginRequired(JWT_SECRET), HorariosValidator.update, horariosController.update)
    .get(loginRequired(JWT_SECRET), HorariosValidator.details, horariosController.details)


  // TODO: adaptar as novas estruturas de banco
  // app.route('/query/:cep')
  //   .get(queryController.get)
  app.route('/delivery_options/cd/:cdId')
    .get(queryController.getByCD)

  app.route('/cd_config_fields/')
    .get(trasportadoraConfigFieldsController.list)
  app.route('/tipos_entrega/')
    .get(tiposTransporteController.list)

  // TODO: horarios


  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))

  app.use(errors())
  app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
  });


  return app
}



module.exports = createApp