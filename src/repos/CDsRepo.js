const connection = require("../database/connection");
const { errorResult, successResult } = require("../utils/result");

const create = async (cdData) => {
  try {
    const {description, id} = cdData;
  
    await connection('cds')
      .insert({description, id})
  
    return successResult(await connection('cds')
      .select('id', 'description')
      .where({id})
      .first())
  } catch (err) {
    if (err?.code === 'ER_DUP_ENTRY') {
      return errorResult('Registro duplicado.')
    }
    return errorResult('Erro ao salvar em banco de dados.')
  }
}

const list = async () => {
  return await connection('cds')
    .select('id', 'description')
}

const getById = async (id) => {
  return await connection('cds')
    .select('id', 'description')
    .where({id})
    .first()
}

const update = async (id, cdData) => {
  await connection('cds')
    .where({id})
    .update(cdData)
  return await connection('cds')
    .select('id', 'description')
    .where({id})
    .first()
}

const deleteCD = async (id) => {
  await connection('cds')
    .where({id})
    .delete()
}

const addBrick = async (cd, brick, config_id) => {
  await connection('bricks_has_cds')
    .insert({
      cd_id: cd.id,
      brick_id: brick.id,
      cd_config_id: config_id
    })
}

const getBricks = async (cd) => {
  return await connection('bricks')
  .select('id', 'name', 'description')
    .join('bricks_has_cds as chb', 'bricks.id', '=', 'chb.brick_id')
    .where({cd_id: cd.id})
}

const deleteBrick = async (cd, brick) => {
  await connection('bricks_has_cds')
    .where({
      cd_id: cd.id,
      brick_id: brick.id,
    })
    .delete()
}


module.exports = {
  create,
  list,
  update,
  delete: deleteCD,
  addBrick,
  getBricks,
  deleteBrick,
  getById,
}