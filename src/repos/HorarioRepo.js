const connection = require("../database/connection")

const diaDaSemanaMapToInt = (dia) => {
  const mapObject = {
    domingo: 1,
    segunda: 2,
    terca: 3,
    quarta: 4,
    quinta: 5,
    sexta: 6,
    sabado: 7,
  }
  return mapObject[dia]
}
const mapIntToDiaSemana = (diaInt) => {
  const mapObject = [
    'domingo', 'segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sabado'
  ]
  return mapObject[diaInt - 1]
}

const mapDiasDaSemanaFromDatabase = (row) => ({
  periodos: row.periodos,
  dia: mapIntToDiaSemana(row.dia_semana),
})

const create = async (horarioData) => {
  const { intervalo, descricao, dias_da_semana, use_intervalo } = horarioData

  const [id] = await connection('horarios_de_operacao_cd')
    .insert({ intervalo, descricao, use_intervalo })

  await connection('horarios_de_operacao_dia_semana')
    .insert(dias_da_semana.map(d => ({
      dia_semana: diaDaSemanaMapToInt(d.dia), horario_id: id,
      periodos: JSON.stringify(d.periodos),
    })))

  const diasDaSemanaDatabase = await connection('horarios_de_operacao_dia_semana')
    .where({ horario_id: id })
    .orderBy('dia_semana')

  const horario = await connection('horarios_de_operacao_cd').where({ id }).first()
  
  return {...horario, dias_da_semana: diasDaSemanaDatabase.map(mapDiasDaSemanaFromDatabase)}
}

const list = async () => {

  const records = await connection('horarios_de_operacao_cd as h')
    .join('horarios_de_operacao_dia_semana AS sem', 'sem.horario_id', '=', 'h.id')
    .orderBy('h.id', 'sem.dia_semana')
  
  const horarios = []
  const index = {}

  records.map(r => {
    const {id} = r
    let pos = index[id]
    if (pos === undefined) {
      pos = horarios.length;
      horarios.push({
        id, descricao: r.descricao, intervalo: r.intervalo, use_intervalo: r.use_intervalo, dias_da_semana: []
      })
      index[id] = pos
    }
    horarios[pos].dias_da_semana.push({
      periodos: r.periodos, dia: mapIntToDiaSemana(r.dia_semana)
    })
  })
  
  return horarios
}

const details = async (id) => {

  const diasDaSemanaDatabase = await connection('horarios_de_operacao_dia_semana')
    .where({ horario_id: id })
    .orderBy('dia_semana')

  const horario = await connection('horarios_de_operacao_cd').where({ id }).first()
  
  return {...horario, dias_da_semana: diasDaSemanaDatabase.map(mapDiasDaSemanaFromDatabase)}
}

const update = async (id, horarioData) => {
  const { intervalo, descricao, dias_da_semana, use_intervalo } = horarioData
  const updateCommand = connection('horarios_de_operacao_cd')
  if (intervalo) {
    updateCommand
      .update({ intervalo })
  }
  if (descricao) {
    updateCommand
      .update({ descricao })
  }
  if (use_intervalo) {
    updateCommand
      .update({ use_intervalo })
  }
  if (intervalo || descricao || use_intervalo) {
    await updateCommand
      .where({id})
  }

  if (dias_da_semana?.length) {
    await Promise.all(dias_da_semana.map(async d => {
      if (d.closed) {
        await connection('horarios_de_operacao_dia_semana')
        .delete()
        .where({horario_id: id, dia_semana: diaDaSemanaMapToInt(d.dia)})
      } else {
        await connection('horarios_de_operacao_dia_semana')
          .update({
            periodos: JSON.stringify(d.periodos),
          })
          .where({horario_id: id, dia_semana: diaDaSemanaMapToInt(d.dia)})
      }
    }))

  }


  const diasDaSemanaDatabase = await connection('horarios_de_operacao_dia_semana')
    .where({ horario_id: id })
    .orderBy('dia_semana')

  const horario = await connection('horarios_de_operacao_cd').where({ id }).first()
  
  return {...horario, dias_da_semana: diasDaSemanaDatabase.map(mapDiasDaSemanaFromDatabase)}
}


module.exports = {
  create,
  list,
  details,
  update,
  get: details,
}