const bcrypt = require("bcrypt")

const connection = require('../database/connection')


const getUserByCredentials = async (userCredential) => {
  const user = await connection('users')
    .where({username: userCredential})
    .orWhere({email: userCredential})
    .first()
  return user
}
const create = async (user) => {
  const {username, first_name, last_name, email, password} = user

  const password_hash = bcrypt.hashSync(password, 10);

  const id = await connection('users').insert({
    username, first_name, last_name, email, password_hash
  }).returning('id')

  const userCreated = await connection('users')
    .select([ 'users.username', 'users.first_name', 'users.last_name',
              'users.email', 'users.id'])
    .where({id})
    .first()

  return userCreated
}

const getUserWithCredentialsConflict = async ({username, email}) => {
  const query = connection('users')
  if (username) {
    query.where({username})
  }
  if (email) {
    query.orWhere({email})
  }
  return await query.first()
}

const getById = async (id) => {
  const user = await connection('users')
    .select(['users.id', 'users.first_name', 'users.last_name', 'users.username', 'users.email'])
    .where({ id }).first()
  return user
}

const update = async(id, userData) => {
  await connection('users')
    .where({ id })
    .update(userData)
}



module.exports = {
  getUserByCredentials,
  create,
  getUserWithCredentialsConflict,
  getById,
  update
}