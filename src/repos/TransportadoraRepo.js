const connection = require("../database/connection");

const create = async (transportadoraData) => {
  const {tipo_pessoa, cnpj, inscricao_estadual, razao_social, nome_fantasia,
    cep, rua, numero, complemento, bairro, cidade, estado, email, telefone, observacoes} = transportadoraData;

  await connection('transportadoras')
    .insert({
      tipo_pessoa, cnpj, inscricao_estadual, razao_social, nome_fantasia, cep, rua, numero, complemento, bairro, cidade, estado,
      email, telefone, observacoes
    })

  return await connection('transportadoras')
    .select('id', 'tipo_pessoa', 'cnpj', 'inscricao_estadual', 'razao_social',
    'nome_fantasia', 'cep', 'rua', 'numero', 'complemento', 'bairro', 'cidade', 'estado',
    'email', 'telefone', 'observacoes')
    .where({cnpj})
    .first()
}

const list = async () => {
  return await connection('transportadoras')
  .select('id', 'tipo_pessoa', 'cnpj', 'inscricao_estadual', 'razao_social',
    'nome_fantasia', 'cep', 'rua', 'numero', 'complemento', 'bairro', 'cidade', 'estado',
    'email', 'telefone', 'observacoes')
}

const getById = async (transportadoraId) => {
  return await connection('transportadoras')
    .where({id: transportadoraId})
    .first()
}

const getByCnpj = async (cnpj) => {
    return await connection('transportadoras')
      .where({cnpj})
      .first()
  }

  const getByRazaoSocial = async (razao_social) => {
    return await connection('transportadoras')
      .where({razao_social})
      .first()
  }

const update = async (transportadoraId, transportadoraData) => {
  await connection('transportadoras')
    .where({id: transportadoraId})
    .update(transportadoraData)
  return await connection('transportadoras')
    .where({id: transportadoraId})
    .select('id', 'tipo_pessoa', 'cnpj', 'inscricao_estadual', 'razao_social',
    'nome_fantasia', 'cep', 'rua', 'numero', 'complemento', 'bairro', 'cidade', 'estado',
    'email', 'telefone', 'observacoes')
    .first()
}

const deleteTransportadora = async (transportadoraId) => {
  await connection('transportadoras')
    .where({id: transportadoraId})
    .delete()
}

module.exports = {
  create,
  list,
  update,
  getByCnpj,
  getByRazaoSocial,
  delete: deleteTransportadora,
  getById,
}