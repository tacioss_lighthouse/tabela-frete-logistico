const connection = require("../database/connection");
const { errorResult, successResult } = require("../utils/result");

const horarioRepo = require("./HorarioRepo")

const create = async (configData) => {
  const { descricao, observacoes, props } = configData;

  const [id] = await connection('cd_config')
  .insert({ descricao, observacoes, props: JSON.stringify(props) })

  const config = await getById(id)

  return config

}

const listByBrickAndCD = async () => {
  const configs = await connection('cd_config')
    .select('id', 'descricao', 'observacoes', 'props')
    // .where({ cd_id, brick_id })

  return configs
}

const update = async (configId, configData) => {
  const { descricao, observacoes, props, tipo_transporte_id } = configData;

  const command = connection('cd_config')
  if (descricao) {
    command.update({ descricao })
  }
  if (observacoes) {
    command.update({ observacoes })
  }
  if (tipo_transporte_id) {
    command.update({ tipo_transporte_id })
  }

  const propsToDelete = []
  const propsToUpdate = []

  if (props) {
    Object.entries(props).map(([key, value]) => {
      if (value === null) {
        propsToDelete.push(key)
        console.log('delete', key)
      } else {
        propsToUpdate.push([key, value])
        console.log('update', key, value)
      }
    })
    const propsToUpdateQuery = propsToUpdate.map(p => `'$.${p[0]}', ?`).join(', ')
    const propsToUpdateValues = propsToUpdate.map(p => p[1])
    console.log({propsToUpdate})
    
    command.update({
      props: connection.raw(`JSON_SET(props, ${propsToUpdateQuery})`, propsToUpdateValues)
    })
  }
  
  command.where({ id: configId })
  
  // console.log({propsToDelete})
  console.log(command.toSQL().toNative())
  
  await command
  if (propsToDelete.length) {
    const propsToDeleteQuery = propsToDelete.map(p => `'$.${p}'`).join(', ')
    await connection('cd_config').update({
      props: connection.raw(`JSON_REMOVE(props, ${propsToDeleteQuery})`)
    }).where({ id: configId })
  }

  const config = await getById(configId)

 return config
}

const deleteConfig = async (id) => {
  await connection('cd_config')
    .delete()
    .where({ id })
}

const getPropsByConfigId = async (configId, transaction = connection) => {
  const propsRecords = await transaction('config_tranportadora_props').where({ config_tranportadora_id: configId })

  const props = propsRecords.reduce((acc, record) => ({ ...acc, [record.key]: record.value }), {})
  return props
}


    
const getById = async (id) => {
  return await connection('cd_config')
    .select('id', 'descricao', 'observacoes', 'props')
    .where({ id })
    .first()

}

const getTransportadoras = async (config) => {
  return await connection('transportadoras')
    .select('id', 'tipo_pessoa', 'cnpj', 'inscricao_estadual', 'razao_social',
      'nome_fantasia', 'cep', 'rua', 'numero', 'complemento', 'bairro', 'cidade', 'estado',
      'email', 'telefone', 'observacoes')
    .join('cd_config_has_transportadoras as cht', 'transportadoras.id', '=', 'cht.transportadora_id')
    .where({cd_config_id: config.id})
}

const addTransportadora = async (config, transportadora) => {
  try {
    await connection('cd_config_has_transportadoras')
      .insert({
        cd_config_id: config.id,
        transportadora_id: transportadora.id,
      })
  } catch (err) {
    if (err?.code === 'ER_DUP_ENTRY') {
      return errorResult('Registro duplicado.')
    }
    return errorResult('Erro ao salvar em banco de dados.')
  }
}

const deleteTransportadora = async (config, transportadora) => {
  await connection('cd_config_has_transportadoras')
    .where({
      cd_config_id: config.id,
      transportadora_id: transportadora.id,
    })
    .delete()
}

const addTransporte = async (configId, valor, tipo_transporte_id, tempo_entrega, horario_id) => {
  const tipo_transporte = await connection('tipos_tranporte').where({id: tipo_transporte_id}).first()
  try {
    await connection('cd_config_has_tipo_transporte_table')
      .insert({
        cd_config_id: configId, valor, tipo_transporte_id, tempo_entrega, horario_id
      })
  } catch (err) {
    if (err?.code === 'ER_DUP_ENTRY') {
      return errorResult('Registro duplicado.')
    }
    console.log(err)
    return errorResult('Erro ao salvar em banco de dados.')
  }
  return successResult({
    config_id: configId, valor, tipo_transporte
  })
}

const updateTransporte = async (configId, valor, tipo_transporte_id, tempo_entrega, horario_id) => {
  const tipo_transporte = await connection('tipos_tranporte').where({id: tipo_transporte_id}).first()
  await connection('cd_config_has_tipo_transporte_table')
    .update({
      cd_config_id: configId, valor, tipo_transporte_id, tempo_entrega, horario_id
    })
    .where({tipo_transporte_id, cd_config_id: configId})
  return {
    config_id: configId, valor, tipo_transporte, horario_id
  }
}
const showTransporte = async (configId, tipo_transporte_id) => {
  const record = await connection('cd_config_has_tipo_transporte_table')
    .where({cd_config_id: configId, tipo_transporte_id}).first()
  if(!record) {
    return errorResult('Not Found')
  }
  const tipo_transporte = await connection('tipos_tranporte').where({id: tipo_transporte_id}).first()
  
  return successResult({
    config_id: configId, valor: record.valor, tipo_transporte, tempo_entrega: record.tempo_entrega, horario_id: record.horario_id
  })
}
const listTransporte = async (configId) => {

  const record = await connection('cd_config_has_tipo_transporte_table')
    .where({cd_config_id: configId})
  const transportes = await Promise.all(record.map(async r => {
    const tipo_transporte = await connection('tipos_tranporte').where({id: r.tipo_transporte_id}).first()
    // const horario = r?.horario_id ? await horarioRepo.get(r.horario_id): null
    return {
      config_id: configId, valor: r.valor, tipo_transporte, horario_id: r.horario_id
    }
  }))
  return transportes
}

const listManyTransporte = async (configIds) => {

  const record = await connection('cd_config_has_tipo_transporte_table')
    .whereIn('cd_config_id', configIds)
  const transportes = await Promise.all(record.map(async r => {
    const tipo_transporte = await connection('tipos_tranporte').where({id: r.tipo_transporte_id}).first()
    // const horario = r?.horario_id ? await horarioRepo.get(r.horario_id): null
    return {
      config_id: r.cd_config_id, valor: r.valor, tipo_transporte, horario_id: r.horario_id
    }
  }))
  return transportes
}


module.exports = {
  create,
  update,
  delete: deleteConfig,
  getPropsByConfigId,
  getById,
  listByBrickAndCD,
  addTransportadora,
  getTransportadoras,
  deleteTransportadora,
  addTransporte,
  listTransporte,
  updateTransporte,
  showTransporte,
  listManyTransporte,
}
