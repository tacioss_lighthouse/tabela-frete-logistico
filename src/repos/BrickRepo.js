const connection = require("../database/connection");

const create = async (brickData) => {
  const {name, description} = brickData;

  const [id] = await connection('bricks')
    .insert({name, description}, 'id')

  return await connection('bricks')
    .select('id', 'name', 'description')
    .where({id})
    .first()
}

const list = async () => {
  return await connection('bricks')
    .select('id', 'name', 'description')
}

const getById = async (id) => {
  return await connection('bricks')
    .select('id', 'name', 'description')
    .where({id})
    .first()
}

const getByName = async (name) => {
  return await connection('bricks')
    .where({name})
    .first()
}

const update = async (id, brickData) => {
  await connection('bricks')
    .where({id})
    .update(brickData)
    return await connection('bricks')
    .select('id', 'name', 'description')
    .where({id})
    .first()
}

const deleteBrick = async (id) => {
  await connection('bricks')
    .where({id})
    .delete()
}

const addLogradouro = async (brick, logradouro) => {
  await connection('bricks_operates_logradouros')
    .insert({
      brick_id: brick.id,
      logradouro_id: logradouro.id
    })
}

const getLogradouros = async (brick) => {
  return await connection('logradouros')
    .select('id', 'cep', 'logradouro', 'bairro', 'cidade', 'uf')
    .join('bricks_operates_logradouros as bol', 'logradouros.id', '=', 'bol.logradouro_id')
    .where({brick_id: brick.id})
}

const deleteLogradouro = async (brick, logradouro) => {
  await connection('bricks_operates_logradouros')
  .where({
    brick_id: brick.id,
      logradouro_id: logradouro.id
    })
    .delete()
}

const getCds = async (brick) => {
  return await connection('cds')
    .select('id', 'description')
    .join('bricks_has_cds as chb', 'cds.id', '=', 'chb.cd_id')
    .where({brick_id: brick.id})
}

module.exports = {
  create,
  list,
  update,
  getById,
  getByName,
  delete: deleteBrick,
  addLogradouro,
  getLogradouros,
  deleteLogradouro,
  getCds,
}