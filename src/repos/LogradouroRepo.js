const connection = require("../database/connection");

const create = async (cepData) => {
  const {cep, logradouro, bairro, cidade, uf} = cepData;

  await connection('logradouros')
    .insert({
      cep: cep.replaceAll(/[^0-9]/g, ''),
      logradouro, bairro, cidade, uf
    })

  return await connection('logradouros')
    .select('id', 'cep', 'logradouro', 'bairro', 'cidade', 'uf')
    .where({cep})
    .first()
}

const list = async () => {
  return await connection('logradouros')
    .select('id', 'cep', 'logradouro', 'bairro', 'cidade', 'uf')
}

const getByCEP = async (cep) => {
  return await connection('logradouros')
    .where({cep})
    .first()
}

const getById = async (logradouroId) => {
  return await connection('logradouros')
    .where({id: logradouroId})
    .first()
}

const update = async (logradouroId, cepData) => {
  await connection('logradouros')
    .where({id: logradouroId})
    .update(cepData)
  return await connection('logradouros')
    .where({id: logradouroId})
    .select('id', 'cep', 'logradouro', 'bairro', 'cidade', 'uf')
    .first()
}

const deleteLogradouro = async (logradouroId) => {
  await connection('logradouros')
    .where({id: logradouroId})
    .delete()
}

const search = async (searchObject) => {
  const {cep} = searchObject
  const query = connection('logradouros')
    .select('id', 'cep', 'logradouro', 'bairro', 'cidade', 'uf')

  if(cep){
    query.where('cep', 'like', `%${cep.replace('-', '')}%`)
  }
  ['logradouro', 'bairro', 'cidade', 'uf'].map((key) => {
    if (searchObject[key]) {
      query.where(key, 'like', `%${searchObject[key]}%`)
    }
  })

  return query

}


module.exports = {
  create,
  list,
  update,
  delete: deleteLogradouro,
  getByCEP,
  getById,
  search
}