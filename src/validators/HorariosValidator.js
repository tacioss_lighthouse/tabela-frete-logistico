const { celebrate, Joi, Segments } = require('celebrate')


module.exports = {
  create: celebrate({
    [Segments.BODY]: Joi.object().keys({
      horario: Joi.object().keys({
        descricao: Joi.string().required(),
        // TODO: fancy message
        use_intervalo: Joi.bool().default(true),
        intervalo: Joi.string().required().regex(/(\d{0,2}[h])*(\d{0,2}[m])*/),
        dias_da_semana: Joi.array().items(Joi.object().keys({
          dia: Joi.string().required().valid('domingo', 'segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sabado'),
          periodos: Joi.array().items(Joi.object().keys({
            nome: Joi.string(),
            inicio: Joi.string().required().regex(/(\d{0,2}[h])*(\d{0,2}[m])*/),
            fim: Joi.string().required().regex(/(\d{0,2}[h])*(\d{0,2}[m])*/),
          })).required()
        })).required().min(1).max(7).unique((a, b) => a.dia === b.dia)
      }).required()
    })
  }),
  update: celebrate({
    [Segments.BODY]: Joi.object().keys({
      horario: Joi.object().keys({
        descricao: Joi.string(),
        // TODO: fancy message
        intervalo: Joi.string().regex(/(\d{0,2}[h])*(\d{0,2}[m])*/),
        use_intervalo: Joi.bool(),
        dias_da_semana: Joi.array().items(Joi.object().keys({
          dia: Joi.string().required().valid('domingo', 'segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sabado'),
          closed: Joi.bool().default(false),
          periodos: Joi.array().items(Joi.object().keys({
            nome: Joi.string(),
            inicio: Joi.string().required().regex(/(\d{0,2}[h])*(\d{0,2}[m])*/),
            fim: Joi.string().required().regex(/(\d{0,2}[h])*(\d{0,2}[m])*/),
          })).required()
        })).min(0).max(7).unique((a, b) => a.dia === b.dia)
      }).required().min(1)
    })
  }),
  delete: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required()
    }),
  }),
  details: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      id: Joi.number().integer().required()
    }),
  }),
}