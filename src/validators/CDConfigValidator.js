const { celebrate, Joi, Segments } = require('celebrate')


module.exports = {
  create: celebrate({
    // [Segments.PARAMS]: Joi.object().keys({
    //   cdId: Joi.number().integer().required(),
    //   brickId: Joi.number().integer().required(),
    // }),
    [Segments.BODY]: Joi.object().keys({
      config: Joi.object().keys({
        descricao: Joi.string().required(),
        observacoes: Joi.string(),
        props: Joi.object().required(),
      }).required()
    }).required()
  }),
  // list: celebrate({
  //   [Segments.PARAMS]: Joi.object().keys({
  //     cdId: Joi.number().integer().required(),
  //     brickId: Joi.number().integer().required(),
  //   }),
  // }),
  get: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
    }),
  }),
  update: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
      config: Joi.object().keys({
        descricao: Joi.string(),
        observacoes: Joi.string(),
        props: Joi.object(),
      }).min(1)
    }).required()
  }),
  delete: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
    }),
  }),
  addTransportadora: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
      transportadoraId: Joi.number().integer().required(),
    }),
  }),
  listTransportadoras: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required()
    }),
  }),
  deleteTransportadora: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
      transportadoraId: Joi.number().integer().required(),
    }),
  }),
  addTransporte: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
      transporte: Joi.object().keys({
        valor: Joi.number().required(),
        horario_id: Joi.number().required(),
        tipo_transporte_id: Joi.number().required(),
        tempo_entrega: Joi.string().required(),
      }).min(1)
    }).required(),
  }),
  updateTransporte: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
      transporteId: Joi.number().integer().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
      transporte: Joi.object().keys({
        tempo_entrega: Joi.string(),
        horario_id: Joi.number(),
        valor: Joi.number(),
      }).min(1)
    }).required(),
  }),
  showTransporte: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
      transporteId: Joi.number().integer().required(),
    }),
  }),
  listTransporte: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      configId: Joi.number().integer().required(),
    }),
  }),
}