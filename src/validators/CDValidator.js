const { celebrate, Joi, Segments } = require('celebrate')


module.exports = {
  create: celebrate({
    [Segments.BODY]: Joi.object().keys({
      cd: Joi.object().keys({
        id: Joi.number().integer().required(),
        description: Joi.string().required(),
      }).required()
    })
  }),
  update: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required()
    }),
    [Segments.BODY]: Joi.object().keys({
      cd: Joi.object().keys({
        description: Joi.string(),
      }).required().min(1)
    }).required()
  }),
  delete: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required()
    }),
  }),
  get: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required()
    }),
  }),
  addBrick: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required(),
      brickId: Joi.number().integer().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
      config_id: Joi.number().integer().required(),
    }),
  }),
  deleteBrick: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required(),
      brickId: Joi.number().integer().required(),
    }),
  }),
  listBricks: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      cdId: Joi.number().integer().required()
    }),
  }),
  listCdsByBrick: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required()
    }),
  }),
}