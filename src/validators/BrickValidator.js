const { celebrate, Joi, Segments } = require('celebrate')


module.exports = {
  create: celebrate({
    [Segments.BODY]: Joi.object().keys({
      brick: Joi.object().keys({
        name: Joi.string().required(),
        description: Joi.string().required(),
      }).required()
    })
  }),
  update: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required()
    }),
    [Segments.BODY]: Joi.object().keys({
      brick: Joi.object().keys({
        name: Joi.string(),
        description: Joi.string(),
      }).required().min(1)
    }).required()
  }),
  delete: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required()
    }),
  }),
  get: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required()
    }),
  }),
  addLogradouro: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required(),
      logradouroId: Joi.number().integer().required(),
    }),
  }),
  deleteLogradouro: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required(),
      logradouroId: Joi.number().integer().required(),
    }),
  }),
  listLogradouros: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      brickId: Joi.number().integer().required()
    }),
  }),
}