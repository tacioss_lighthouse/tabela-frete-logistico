const { celebrate, Joi, Segments } = require('celebrate')


module.exports = {
  create: celebrate({
    [Segments.BODY]: Joi.object().keys({
      transportadora: Joi.object().keys({
        tipo_pessoa: Joi.string().required(),
        cnpj: Joi.string().required(),
        inscricao_estadual: Joi.number().integer(),
        razao_social: Joi.string().required(),
        nome_fantasia: Joi.string(),
        cep: Joi.string().max(9),
        rua: Joi.string().required(),
        numero: Joi.string().required(),
        complemento: Joi.string(),
        bairro: Joi.string(),
        cidade: Joi.string().required(),
        estado: Joi.string().required(),
        email: Joi.string().required(),
        telefone: Joi.string().required(),
        observacoes: Joi.string(),
      }).required()
    })
  }),
  update: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      transportadoraId: Joi.number().integer().required()
    }),
    [Segments.BODY]: Joi.object().keys({
      transportadora: Joi.object().keys({
        tipo_pessoa: Joi.string().required(),
        cnpj: Joi.string().required(),
        inscricao_estadual: Joi.number().integer(),
        razao_social: Joi.string().required(),
        nome_fantasia: Joi.string(),
        cep: Joi.string().max(9),
        rua: Joi.string().required(),
        numero: Joi.string().required(),
        complemento: Joi.string(),
        bairro: Joi.string(),
        cidade: Joi.string().required(),
        estado: Joi.string().required(),
        email: Joi.string().required(),
        telefone: Joi.string().required(),
        observacoes: Joi.string(),
      }).required().min(1)
    }).required()
  }),
  delete: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
        transportadoraId: Joi.number().integer().required()
    }),
  }),
  get: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
        transportadoraId: Joi.number().integer().required()
    }),
  }),
}