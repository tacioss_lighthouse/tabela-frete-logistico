const { celebrate, Joi, Segments } = require('celebrate')


module.exports = {
  create: celebrate({
    [Segments.BODY]: Joi.object().keys({
      logradouro: Joi.object().keys({
        cep: Joi.string().max(9).required(),
        logradouro: Joi.string().required(),
        bairro: Joi.string().required(),
        cidade: Joi.string().required(),
        uf: Joi.string().required(),
      }).required()
    })
  }),
  update: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      logradouroId: Joi.number().integer().required()
    }),
    [Segments.BODY]: Joi.object().keys({
      logradouro: Joi.object().keys({
        cep: Joi.string().max(9),
        logradouro: Joi.string(),
        bairro: Joi.string(),
        cidade: Joi.string(),
        uf: Joi.string(),
      }).required().min(1)
    }).required()
  }),
  delete: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      logradouroId: Joi.number().integer().required()
    }),
  }),
  get: celebrate({
    [Segments.PARAMS]: Joi.object().keys({
      logradouroId: Joi.number().integer().required()
    }),
  }),
  search: celebrate({
    [Segments.QUERY]: Joi.object().keys({
      cep: Joi.string().max(9),
      logradouro: Joi.string(),
      bairro: Joi.string(),
      cidade: Joi.string(),
      uf: Joi.string(),
    }).required().min(1),
  }),
}