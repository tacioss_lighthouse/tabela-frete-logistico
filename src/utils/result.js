const successResult = (data, extra) => ({
  success: true,
  data,
  ...extra,
});
const errorResult = (message, extra) => ({
  success: false,
  message,
  ...extra,
});
module.exports = { successResult, errorResult };

// errorResult(...).type(NotFound).extras({...})
// {errorFlag: true, data, message, error}