
exports.up = async function(knex) {
  await knex.schema.createTable('cd_config', table => {
    table.increments('id')

    table.string('descricao', 80)
    table.text('observacoes')
    table.jsonb('props').notNullable()
  })
  await knex.schema.createTable('cd_config_has_transportadoras', table => {
    table.integer('cd_config_id').unsigned().notNullable()
      .references('id').inTable('cd_config')
    table.integer('transportadora_id').unsigned().notNullable()
      .references('id').inTable('transportadoras')
    table.primary(['cd_config_id', 'transportadora_id'])
  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('cd_config_has_transportadoras')
  await knex.schema.dropTable('cd_config')
};

/*

MOSTA AGENDA e em tipo de transporte

in = cd

out =

{
  options: [
    {
      props: {},
      transportadoras: [],

      tipo_entrega: {
        name: normal,
        type: rapida,
        mostra agenda: nao,
        valor: 45,
      },
    },
    {
      props: {},
      transportadoras: [],

      tipo_entrega: {
        name: agendada,
        type: agendada,
        mostra agenda: sim,
        valor: 45,
        agenda: {
          tempo: 1h, // definir um nome melhor,
          dias: {
            segunda: [
              {
                start: 8,
                end: 12
              },
              {
                start: 13,
                end: 18
              }
            ],
            ...
          }
        }
      },
    }

  ]
}




*/