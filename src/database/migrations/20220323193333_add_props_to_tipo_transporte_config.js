
exports.up = function(knex) {
  return knex.schema.table('cd_config_has_tipo_transporte_table', table => {
    table.string('tempo_entrega', 20).defaultTo('1h')
  })
};

exports.down = function(knex) {
  return knex.schema.table('cd_config_has_tipo_transporte_table', table => {
    table.dropColumn('tempo_entrega')
  })
};
