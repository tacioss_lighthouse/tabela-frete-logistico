
exports.up = async function(knex) {
  await knex.schema.createTable('cds', table => {
    table.integer('id').unsigned().notNullable()
    table.primary('id')
    table.string('description', 45).notNullable()
  })
  await knex.schema.createTable('bricks_has_cds', table => {
    table.integer('brick_id').unsigned().notNullable()
      .references('id').inTable('bricks')
    table.integer('cd_id').unsigned().notNullable()
      .references('id').inTable('cds')
    table.integer('cd_config_id').unsigned().notNullable()
      .references('id').inTable('cd_config')
    table.primary(['brick_id', 'cd_id'])
  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('bricks_has_cds')
  await knex.schema.dropTable('cds')
};
