
exports.up = async function(knex) {
  await knex.schema.createTable('horarios_de_operacao_cd', table => {
    table.increments('id')
    table.string('descricao', 80).notNullable()
    table.string('intervalo', 6)
  })
  await knex.schema.createTable('horarios_de_operacao_dia_semana', table => {
    table.integer('dia_semana', 2).notNullable()
    // [{inicio: 8h, fim: 18h}]
    table.jsonb('periodos').notNullable()
    table.integer('horario_id').unsigned().notNullable()
    .references('id').inTable('horarios_de_operacao_cd')
    table.primary(['horario_id', 'dia_semana'])
  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('horarios_de_operacao_dia_semana')
  await knex.schema.dropTable('horarios_de_operacao_cd')
};