
exports.up = async function(knex) {
  await knex.schema.createTable('cd_config_has_tipo_transporte_table', table => {

    table.float('valor').notNullable()

    table.integer('tipo_transporte_id').unsigned().notNullable()
      .references('id').inTable('tipos_tranporte')
    table.integer('cd_config_id').unsigned().notNullable()
      .references('id').inTable('cd_config')
    table.primary(['cd_config_id', 'tipo_transporte_id'])

  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('cd_config_has_tipo_transporte_table')
};