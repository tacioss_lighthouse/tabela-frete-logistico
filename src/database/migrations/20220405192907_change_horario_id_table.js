
exports.up = async function(knex) {
  await knex.schema.table('cd_config_has_tipo_transporte_table', table => {
    table.integer('horario_id').unsigned().notNullable().default(1)
      .references('id').inTable('horarios_de_operacao_cd')
  })
  await knex.schema.table('tipos_tranporte', table => {
    table.dropForeign('horario_id')
    table.dropColumn('horario_id')
  })
};

exports.down = async function(knex) {
  await knex.schema.table('cd_config_has_tipo_transporte_table', table => {
    table.dropForeign('horario_id')
    table.dropColumn('horario_id')
  })
  await knex.schema.table('tipos_tranporte', table => {
    table.integer('horario_id').unsigned().notNullable().default(1)
      .references('id').inTable('horarios_de_operacao_cd')
  })
};
