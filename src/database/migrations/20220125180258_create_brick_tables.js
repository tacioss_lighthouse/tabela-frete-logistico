
exports.up = async function(knex) {
  await knex.schema.createTable('bricks', table => {
    table.increments('id')
    table.string('name', 80).unique()
    table.text('description')
  })
  await knex.schema.createTable('bricks_operates_logradouros', table => {
    table.integer('brick_id').unsigned().notNullable()
      .references('id').inTable('bricks')
    table.integer('logradouro_id').unsigned().notNullable()
      .references('id').inTable('logradouros')
    table.primary(['brick_id', 'logradouro_id'])
  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('bricks_operates_logradouros')
  await knex.schema.dropTable('bricks')
};
