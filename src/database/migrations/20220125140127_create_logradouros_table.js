
exports.up = function(knex) {
  return knex.schema.createTable('logradouros', table => {
    table.increments('id')
    table.string('cep', 8).unique()
    table.string('logradouro', 40).notNullable()
    table.string('bairro', 40).notNullable()
    table.string('cidade', 60).notNullable()
    table.string('uf', 2).notNullable()
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('logradouros');
};


// ['RO', 'AC', 'AM', 'RR', 'PA', 'AP', 'TO', 'MA', 'PI', 'CE', 'RN', 'PB', 'PE', 'AL', 'SE', 'BA', 'MG', 'ES', 'RJ', 'SP', 'PR', 'SC', 'RS', 'MS', 'MT', 'GO', 'DF'