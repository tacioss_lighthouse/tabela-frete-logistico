
exports.up = function(knex) {
  return knex.schema.table('horarios_de_operacao_cd', table => {
    table.bool('use_intervalo').defaultTo(true)
  })
};

exports.down = function(knex) {
  return knex.schema.table('horarios_de_operacao_cd', table => {
    table.dropColumn('use_intervalo')
  })
};
