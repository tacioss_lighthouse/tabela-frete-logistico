
exports.up = async function(knex) {
  await knex.schema.createTable('tipos_tranporte', table => {
    table.increments('id')
    table.string('tipo_transporte', 80)
    table.boolean('show_agenda').notNullable().defaultTo(false)

    table.integer('horario_id').unsigned().notNullable()
      .references('id').inTable('horarios_de_operacao_cd')
  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('tipos_tranporte')
};
