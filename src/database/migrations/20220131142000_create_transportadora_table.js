
exports.up = async function(knex) {
  await knex.schema.createTable('transportadoras', table => {
    table.increments('id')
    table.string('tipo_pessoa', 80)
    table.string('cnpj', 80).unique()
    table.string('inscricao_estadual', 80)
    table.string('razao_social', 80)
    table.string('nome_fantasia', 80)
    table.string('cep', 8).notNullable()
    table.string('rua', 40).notNullable()
    table.string('numero', 10).notNullable()
    table.string('complemento', 20).notNullable()
    table.string('bairro', 40).notNullable()
    table.string('cidade', 60).notNullable()
    table.string('estado', 40).notNullable()
    table.string('email').notNullable()
    table.string('telefone').notNullable()
    table.text('observacoes')
  })

};

exports.down = async function(knex) {
  await knex.schema.dropTable('transportadoras')
};
