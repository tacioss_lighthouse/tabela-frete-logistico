
const knex = require('knex')
const knexSettings = require('../../knexfile')

let connection = null
if (process.env.NODE_ENV === "production") {
  connection = knex(knexSettings.production)
} else {
  connection = knex(knexSettings.development)
}

module.exports = connection