
const das8as22 = {inicio: "08h", fim: "22h"}
const das8as21 = {inicio: "08h", fim: "21h"}
const das8as21e30 = {inicio: "08h", fim: "21h30"}

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('horarios_de_operacao_cd').del()
    .then(function () {
      return knex('horarios_de_operacao_dia_semana').del()
    })
    .then(function () {
      // Inserts seed entries
      return knex('horarios_de_operacao_cd').insert([
        {id: 1, descricao: 'Comercial Padrão', intervalo: "1h"},
        {id: 2, descricao: 'Comercial Reduzido', intervalo: "1h"},
        {id: 3, descricao: 'Comercial Reduzido Complemento', intervalo: "1h"},
      ]).then(function () {
          return knex('horarios_de_operacao_dia_semana').del()
            .then(function () {
              // Inserts seed entries
              return knex('horarios_de_operacao_dia_semana').insert([
                {horario_id: 1, dia_semana: 1, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 1, dia_semana: 2, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 1, dia_semana: 3, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 1, dia_semana: 4, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 1, dia_semana: 5, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 1, dia_semana: 6, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 1, dia_semana: 7, periodos: JSON.stringify([das8as21e30])},
                {horario_id: 2, dia_semana: 2, periodos: JSON.stringify([das8as22])},
                {horario_id: 2, dia_semana: 3, periodos: JSON.stringify([das8as22])},
                {horario_id: 2, dia_semana: 4, periodos: JSON.stringify([das8as22])},
                {horario_id: 2, dia_semana: 5, periodos: JSON.stringify([das8as22])},
                {horario_id: 2, dia_semana: 6, periodos: JSON.stringify([das8as22])},
                {horario_id: 3, dia_semana: 1, periodos: JSON.stringify([das8as21])},
              ]);
        });
      });
    });
};
