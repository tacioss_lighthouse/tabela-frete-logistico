
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('tipos_tranporte').del()
    .then(function () {
      // Inserts seed entries
      return knex('tipos_tranporte').insert([
        {id: 1, tipo_transporte: 'balcao',              show_agenda: false, horario_id: 1},
        {id: 2, tipo_transporte: 'rapida',              show_agenda: false, horario_id: 1},
        {id: 3, tipo_transporte: 'programada',          show_agenda: true,  horario_id: 2},
        {id: 4, tipo_transporte: 'balcao_reduzida',     show_agenda: false, horario_id: 2},
        {id: 5, tipo_transporte: 'rapida_reduzida',     show_agenda: false, horario_id: 2},
        {id: 6, tipo_transporte: 'programada_reduzida', show_agenda: true,  horario_id: 2},
      ]);
    });
};
