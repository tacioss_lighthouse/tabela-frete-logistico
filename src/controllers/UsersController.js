const service = require("../services/users")
const connection = require('../database/connection')

const create = async (request, response) => {
  // #swagger.tags = ['Users']
  // #swagger.description = 'Endpoint para criar um User.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['User'] = {
        in: 'body',
        description: 'Informações do User.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddUser" }
    } */
  const userData = request.body.user

  const result = await service.create(userData)

  if (!result.success) {
    if (result.error === 'BadRequest') {
      return response.status(400).json({
        error: result.message
      })
    }
    if (result.error === 'Forbidden') {
      return response.status(403).json({
        error: result.message
      })
    }
    return response.status(500).json(result)
  }
  return response.status(201).json({user: result.data})

}

const update = async (request, response) => {
  // #swagger.tags = ['Users']
  // #swagger.description = 'Endpoint para atualizar um User.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['User'] = {
        in: 'body',
        description: 'Informações do User.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateUser" }
    } */
  const { id } = request.params
  const { username, email } = request.body.user
  const idInToken = request.loggedUserId

  if (idInToken !== parseInt(id)) {
    return response.status(403).send({
      error: 'You can only update your user.'
    })
  }

  const user = await connection('users')
      .select(['users.id', 'users.first_name', 'users.last_name', 'users.username', 'users.email'])
      .where({ id }).first()

  if (!user) {
      return response.status(404).json({
          'error': `User with id #${id} not found.`
      })
  }
  const userAlreadyInDatabase = await getUserWithCredentialsConflict({
    username, email
  })
  if (userAlreadyInDatabase && userAlreadyInDatabase.id !== user.id){
    return response.status(403).json({
      error: 'There is already an user with this username or e-mail.'
    })
  }

  await connection('users')
      .where({ id })
      .update(request.body.user)
  
  const userUpdated = await connection('users')
      .select(['users.id', 'users.first_name', 'users.last_name', 'users.username', 'users.email'])
      .where({ id }).first()

  return response.json({user: userUpdated})
}

const getUserWithCredentialsConflict = async ({username, email}) => {
  const query = connection('users')
  if (username) {
    query.where({username})
  }
  if (email) {
    query.orWhere({email})
  }
  return await query.first()
}

module.exports = {
  create,
  update,
}