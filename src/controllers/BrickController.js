
const repo = require("../repos/BrickRepo");
const service = require("../services/bricks");


const create = async (req, res) => {
  // #swagger.tags = ['Bricks']
  // #swagger.description = 'Endpoint para criar um Brick.'
  /* #swagger.parameters['Brick'] = {
        in: 'body',
        description: 'Informações do Brick.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddBrick" }
    } */
  const brickData = req.body.brick;

  const result = await service.create(brickData)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({brick: result.data})

}
const list = async (req, res) => {
  // #swagger.tags = ['Bricks']
  // #swagger.description = 'Endpoint para listar todos os Bricks.'
  // #swagger.security = [{"bearer": []}]

  return res.json({bricks: await repo.list()})

}
const update = async (req, res) => {
  // #swagger.tags = ['Bricks']
  // #swagger.description = 'Endpoint para atualizar um Brick.'
  // #swagger.security = [{"bearer": []}]
  // #swagger.parameters['brickId'] = { description: 'ID do Brick.', required: true}
  /* #swagger.parameters['Brick'] = {
        in: 'body',
        description: 'Informações do Brick.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateBrick" }
    } */
  const {brickId} = req.params;
  const brickData = req.body.brick;

  const result = await service.update(brickId, brickData)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({brick: result.data})
}
const deleteBrick = async (req, res) => {
  // #swagger.tags = ['Bricks']
  // #swagger.description = 'Endpoint para deletar um Brick.'
  // #swagger.security = [{"bearer": []}]
  // #swagger.parameters['brickId'] = { description: 'ID do Brick.', required: true }
  const {brickId} = req.params;

  const result = await service.delete(brickId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send('')
}

const get = async (req, res) => {
  // #swagger.tags = ['Bricks']
  // #swagger.description = 'Endpoint para obter um Brick.'
  // #swagger.security = [{"bearer": []}]
  // #swagger.parameters['brickId'] = { description: 'ID do Brick.', required: true }
  
  const {brickId} = req.params;

  const result = await service.get(brickId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(200).send(result.data)
}

const addLogradouro = async (req, res) => {
  // #swagger.tags = ['Bricks And Logradouros']
  // #swagger.description = 'Endpoint para adicionar um logradouro a um Brick.'
  // #swagger.security = [{"bearer": []}]
  const {brickId, logradouroId} = req.params;

  const result = await service.addLogradouro(brickId, logradouroId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({brick: result.data})
}
const listLogradouros = async (req, res) => {
  // #swagger.tags = ['Bricks And Logradouros']
  // #swagger.description = 'Endpoint para listar os logradouros de um Brick.'
  // #swagger.security = [{"bearer": []}]
  const {brickId} = req.params
  const result = await service.listLogradouros(brickId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({bricks: result.data})
}
const deleteLogradouro = async (req, res) => {
  // #swagger.tags = ['Bricks And Logradouros']
  // #swagger.description = 'Endpoint para deletar um logradouro de um Brick.'
  // #swagger.security = [{"bearer": []}]
  const {brickId, logradouroId} = req.params;
  const result = await service.deleteLogradouro(brickId, logradouroId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send()
}

module.exports = {
  create,
  list,
  update,
  get,
  deleteBrick,
  addLogradouro,
  listLogradouros,
  deleteLogradouro,
}