const connection = require("../database/connection")
const LogradouroRepo = require("../repos/LogradouroRepo")
const HorarioRepo = require("../repos/HorarioRepo")
const dayjs = require("dayjs")
const utc = require('dayjs/plugin/utc')
const timezone = require('dayjs/plugin/timezone')
dayjs.extend(timezone)
dayjs.extend(utc)

const get = async (req, res) => {
  // #swagger.tags = ['Delivery options']
  // #swagger.description = 'Endpoint para buscar opções de entrega dado um CEP'
  const cep = req.params.cep

  const logradouro = await LogradouroRepo.getByCEP(cep)

  if (!logradouro) {
    return res.status(400).send('Not Found')
  }
  const sql = `
    SELECT
      b.id AS brick_id,
      b.name AS brick_name,
      cds.id_cds AS cds_id,
      cds.description AS cds_description,
      tconfig.id AS transportadora_config_id,
      tconfig.descricao AS transportadora_config_descricao,
      tt.tipo_transporte AS tipo_transporte,
      t.id AS transportadora_id,
      t.tipo_pessoa AS transportadora_tipo_pessoa,
      t.razao_social AS transportadora_razao_social,
      t.nome_fantasia AS transportadora_nome_fantasia
    FROM logradouros AS l
    INNER JOIN bricks_operates_logradouros AS bol ON logradouro_id = l.id
    INNER JOIN bricks AS b ON b.id = bol.brick_id
    INNER JOIN bricks_has_cds AS chb ON chb.brick_id = b.id
    INNER JOIN cds ON chb.cd_id = cds.id_cds
    INNER JOIN transportadoras_operates_bricks AS tob ON tob.brick_id = b.id
    INNER JOIN transportadoras AS t ON t.id = tob.transportadora_id
    INNER JOIN config_tranportadora AS tconfig ON tconfig.id = tob.config_transportadora_id
    INNER JOIN tipos_tranporte AS tt ON tt.id = tconfig.tipo_transporte_id
    WHERE l.cep = ?
  `
  const [records] = await connection.raw(sql, cep)

  const deliveryOptions = []
  const optIndex = {}
  records.map(r => {
    const { transportadora_config_id: id, cds_id } = r
    let index = optIndex[id]
    if (index === undefined) {
      optIndex[id] = deliveryOptions.length
      index = deliveryOptions.length
      const transportadora = {
        id: r.transportadora_id,
        nome_fantasia: r.transportadora_nome_fantasia,
        razao_social: r.transportadora_razao_social,
        tipo_pessoa: r.transportadora_tipo_pessoa,
      }
      const transportadora_config = {
        id: r.transportadora_config_id,
        descricao: r.transportadora_config_descricao,
        tipo_transporte: r.tipo_transporte,
        props: r.transportadora_config_props,
      }
      const brick = { id: r.brick_id, name: r.brick_name }
      deliveryOptions.push({
        transportadora,
        transportadora_config,
        brick,
        cds: []
      })
    }
    if (cds_id) {
      const cd = {
        id: r.cds_id,
        description: r.cds_description
      }
      deliveryOptions[index] = {
        ...deliveryOptions[index],
        cds: [...deliveryOptions[index].cds, cd],
      }
    }
  })
  // const deliveryOptionsWithConfigProps = await Promise.all(deliveryOptions.map(async (opt) => ({
  //   ...opt,
  //   transportadora_config: {
  //     ...opt.transportadora_config,
  //     props: await CDConfigsRepo.getPropsByConfigId(opt.transportadora_config.id)
  //   }
  // })))
  return res.json({ logradouro, delivery_options: deliveryOptions })

}

const parseHorarioToInt = (horario) => {
  const re = /^(?:(\d{0,2})[h]){0,1}(?:(\d{0,2})[m]{0,1}){0,1}$/
  const [trash, hourStr, minStr] = re.exec(horario) || []
  console.log(hourStr, minStr)
  return parseInt(hourStr) * 60 + (parseInt(minStr) || 0)
}
const parseHorarioIntToDate = (horarioInt, midnight) => {
  const hourInt = Math.floor(horarioInt / 60)
  const minInt = horarioInt % 60
  return dayjs(midnight).add(hourInt, 'h').add(minInt, 'm').tz("America/Sao_Paulo").format()
}

const diaDaSemanaMapToInt = (dia) => {
  const mapObject = {
    domingo: 1,
    segunda: 2,
    terca: 3,
    quarta: 4,
    quinta: 5,
    sexta: 6,
    sabado: 7,
  }
  return mapObject[dia]
}

const getDiaDaSemana = (data) => {
  const mapObject = [
    'domingo', 'segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sabado'
  ]
  return mapObject[data.day()]
}

const parseDia = (regras, today, intervalo, increment, useIntervalo) => {
  const dia  = today.add(increment, "day")
  const midnight = dia.startOf("day")
  const nowInMinutes = dia.diff(midnight, "minutes")
  const horarios = []
  if (useIntervalo) {
    const intervaloInt = parseHorarioToInt(intervalo)
    regras.periodos.map(periodo => {
      const inicio = parseHorarioToInt(periodo.inicio)
      const fim = parseHorarioToInt(periodo.fim) - intervaloInt
      for (let i = inicio; i <= fim; i += intervaloInt) {
        if (increment || i >= nowInMinutes + 30) {
          horarios.push({
            start: parseHorarioIntToDate(i, midnight),
            end: parseHorarioIntToDate(i + intervaloInt, midnight)
          })
        }
      }
    })
  } else {
    regras.periodos.map(periodo => {
      if (increment || parseHorarioToInt(periodo.inicio) >= nowInMinutes + 30) {
        horarios.push({
          start: midnight.add(parseHorarioToInt(periodo.inicio), 'minutes').tz("America/Sao_Paulo").format(),
          end: midnight.add(parseHorarioToInt(periodo.fim), 'minutes').tz("America/Sao_Paulo").format(),
          nome: periodo.nome
        })
      }
    })
  }
  return horarios

}

const diffDiaSemana = (regras, index) => {
  oldRegra = regras[index]
  regra = regras[(index + 1) % regras.length]
  increment = diaDaSemanaMapToInt(regra.dia) - diaDaSemanaMapToInt(oldRegra.dia)
  if (increment > 0) {
    return increment
  }
  return increment + 7

}

const parseHorario = (horario) => {
  const { intervalo } = horario
  const now = dayjs().tz('America/Sao_Paulo')
  const diaSemana = getDiaDaSemana(now)
  const firstDayIndex = Math.max(horario.dias_da_semana.findIndex(d => diaDaSemanaMapToInt(d.dia) >= diaDaSemanaMapToInt(diaSemana)) || 0, 0)
  const horariosList = []
  let increment = 0
  for (let c = 0; horariosList.length < 3; c++) {
    const index = (horario.dias_da_semana.length + firstDayIndex + c) % horario.dias_da_semana.length
    const tmp = parseDia(horario.dias_da_semana[index], now, intervalo, increment, horario.use_intervalo)
    if (tmp?.length) {
      horariosList.push({
        horarios: tmp
      })
    }
    increment += diffDiaSemana(horario.dias_da_semana, index)

  }
  return horariosList
}

const getByCD = async (req, res) => {
  // #swagger.tags = ['Delivery options']
  // #swagger.description = 'Endpoint para buscar opções de entrega dado um CD'
  const cdId = req.params.cdId
  console.log(cdId)

  const sql = `
    SELECT tipo_transporte, show_agenda, horario_id, valor, props, tempo_entrega FROM cds
    INNER JOIN bricks_has_cds AS bhc ON bhc.cd_id = cds.id
    INNER JOIN cd_config ON cd_config.id = bhc.cd_config_id
    INNER JOIN cd_config_has_tipo_transporte_table AS t ON t.cd_config_id = bhc.cd_config_id
    INNER JOIN tipos_tranporte ON tipos_tranporte.id = t.tipo_transporte_id
    WHERE cds.id = ?`
  const [records] = await connection.raw(sql, cdId)
  if (!records?.length) {
    return res.status(400).json({ success: false, message: 'Nenhuma opção de entrega encontrada para esse Centro de Distribuição.' })

  }
  const opcoes_entrega = await Promise.all(records.map(async ({ tipo_transporte, show_agenda, horario_id, valor, props, tempo_entrega }) => {
    if (!show_agenda) {
      const retorno = {
        [tipo_transporte]: {
          valor,
          nome: tipo_transporte == 'balcao' ? "Balcão" : "Rápida",
        },
      }
      if (tempo_entrega && tempo_entrega.length) {
        retorno[tipo_transporte].tempo_entrega = tempo_entrega
      } else if (props && props.tempo_entrega_max && props.tempo_entrega_min) {
        retorno[tipo_transporte].tempo_entrega = `${props.tempo_entrega_min} - ${props.tempo_entrega_max}m`
      }
      return retorno
    } else {
      const horario = await HorarioRepo.get(horario_id)
      const dias = parseHorario(horario)
      return {
        programada: {
          nome: "Programada",
          valor,
          dias,
        }
      }
    }
  }))
  const [{ cd_config_id }] = records
  console.log(cd_config_id)
  console.log(opcoes_entrega)
  return res.json({ delivery_options: opcoes_entrega.reduce((acc, el) => ({ ...acc, ...el })) })

  // const tmp = records.map(r => ({
  //   transportadora: {
  //     id: r.transportadora_id,
  //     nome_fantasia: r.transportadora_nome_fantasia,
  //     razao_social: r.transportadora_razao_social,
  //     tipo_pessoa: r.transportadora_tipo_pessoa,
  //   },
  //   transportadora_config: {
  //     id: r.transportadora_config_id,
  //     descricao: r.transportadora_config_descricao,
  //     tipo_transporte: r.tipo_transporte,
  //     props: r.transportadora_config_props,
  //   },
  //   brick: {id: r.brick_id, name: r.brick_name},

  // }))
  // return res.json({ delivery_options: tmp })

}
// const getByCD = async (req, res) => {
//   const cdId = req.params.cdId

//   const sql = `
//     SELECT
//       b.id AS brick_id,
//       b.name AS brick_name,
//       tconfig.id AS transportadora_config_id,
//       tconfig.descricao AS transportadora_config_descricao,
//       tconfig.props AS transportadora_config_props,
//       tt.tipo_transporte AS tipo_transporte,
//       t.id AS transportadora_id,
//       t.tipo_pessoa AS transportadora_tipo_pessoa,
//       t.razao_social AS transportadora_razao_social,
//       t.nome_fantasia AS transportadora_nome_fantasia
//     FROM cds
//     INNER JOIN bricks_has_cds AS chb ON chb.cd_id = cds.id_cds
//     INNER JOIN bricks AS b ON b.id = chb.brick_id
//     INNER JOIN transportadoras_operates_bricks AS tob ON tob.brick_id = b.id
//     INNER JOIN transportadoras AS t ON t.id = tob.transportadora_id
//     INNER JOIN config_tranportadora AS tconfig ON tconfig.id = tob.config_transportadora_id
//     INNER JOIN tipos_tranporte AS tt ON tt.id = tconfig.tipo_transporte_id
//     WHERE cds.id_cds = ?`
//   const [records] = await connection.raw(sql, cdId)

//   const tmp = records.map(r => ({
//     transportadora: {
//       id: r.transportadora_id,
//       nome_fantasia: r.transportadora_nome_fantasia,
//       razao_social: r.transportadora_razao_social,
//       tipo_pessoa: r.transportadora_tipo_pessoa,
//     },
//     transportadora_config: {
//       id: r.transportadora_config_id,
//       descricao: r.transportadora_config_descricao,
//       tipo_transporte: r.tipo_transporte,
//       props: r.transportadora_config_props,
//     },
//     brick: {id: r.brick_id, name: r.brick_name},

//   }))
//   return res.json({ delivery_options: tmp })

// }
module.exports = {
  get,
  getByCD,
}
