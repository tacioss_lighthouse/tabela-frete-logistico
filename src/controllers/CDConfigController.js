const service = require('../services/cdConfigs')

const create = async (req, res) => {
  // #swagger.tags = ['CD Configs']
  // #swagger.description = 'Endpoint para criar uma configuração de CD.'
  /* #swagger.parameters['CDConfig'] = {
        in: 'body',
        description: 'Informações da configuração.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddCDConfig" }
    } */
  const result = await service.create(req.body.config)
  return res.status(201).json({config: result.data})
}

const list = async (req, res) => {
  // #swagger.tags = ['CD Configs']
  // #swagger.description = 'Endpoint para listar todas as configurações de CD'
  // #swagger.security = [{"bearer": []}]
  const result = await service.list()
  return res.status(201).json({configs: result.data})
}

const get = async (req, res) => {
  // #swagger.tags = ['CD Configs']
  // #swagger.description = 'Endpoint para obter uma configuração de CD'
  // #swagger.security = [{"bearer": []}]
  const { configId } = req.params;
  const result = await service.get(configId)
  if (!result.success) {
    return res.status(400).json(result)
  }
  return res.status(200).json({config: result.data})

}

const update = async (req, res) => {
  // #swagger.tags = ['CD Configs']
  // #swagger.description = 'Endpoint para atualizar uma configuração de CD.'
  /* #swagger.parameters['CDConfig'] = {
        in: 'body',
        description: 'Informações da configuração.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateCDConfig" }
    } */
  const { configId } = req.params;
  const result = await service.update(configId, req.body.config)
  return res.status(200).json({config: result.data})
}

const deleteConfig = async (req, res) => {
  // #swagger.tags = ['CD Configs']
  // #swagger.description = 'Endpoint para deletar uma configuração de CD'
  const { configId } = req.params;
  const result = await service.delete(configId)
  if (result.success) {
    return res.status(204).send('')
  }
  return res.status(400).json(result)
}


const addTransportadora = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Transportadoras']
  // #swagger.description = 'Endpoint para adicionar uma transportadora a uma Configuração.'
  const {configId, transportadoraId} = req.params;

  const result = await service.addTransportadora(configId, transportadoraId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({transportadora: result.data})
}

const listTransportadoras = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Transportadoras']
  // #swagger.description = 'Endpoint para listar as transportadoras de uma Configuração.'
  const {configId} = req.params
  const result = await service.listTransportadoras(configId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({transportadoras: result.data})
}

const deleteTransportadora = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Transportadoras']
  // #swagger.description = 'Endpoint para deletar uma transportadora de uma Configuração.'
  const {configId, transportadoraId} = req.params;
  const result = await service.deleteTransportadora(configId, transportadoraId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send()
}

const addTransporte = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Modalides de Entrega']
  // #swagger.description = 'Endpoint para adicionar uma modalidade de entrega de uma Configuração.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['ModalidadeEntrega'] = {
        in: 'body',
        description: 'Informações da configuração.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddCDConfigModalidaDeEntrega" }
    } */
  const {configId} = req.params;
  const {valor, tipo_transporte_id, tempo_entrega, horario_id} = req.body.transporte;

  const result = await service.addTransporte(configId, valor, tipo_transporte_id, tempo_entrega, horario_id)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({transporte: result.data})
}
const updateTransporte = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Modalides de Entrega']
  // #swagger.description = 'Endpoint para atualizar uma modalidade de entrega de uma Configuração.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['ModalidadeEntrega'] = {
        in: 'body',
        description: 'Informações da configuração.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateCDConfigModalidaDeEntrega" }
    } */
  const {configId, transporteId} = req.params;
  const {valor, tempo_entrega, horario_id} = req.body.transporte;

  const result = await service.updateTransporte(configId, valor, transporteId, tempo_entrega, horario_id)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({transporte: result.data})
}
const showTransporte = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Modalides de Entrega']
  // #swagger.description = 'Endpoint para obter uma modalidade de entrega de uma Configuração.'
  // #swagger.security = [{"bearer": []}]
  const {configId, transporteId} = req.params;

  const result = await service.showTransporte(configId, transporteId)

  if(!result.success) {
    return res.status(400).json(result)
  }
  return res.status(200).json({transporte: result.data})
}
const listTransporte = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Modalides de Entrega']
  // #swagger.description = 'Endpoint para listar as modalidades de entrega de uma Configuração.'
  // #swagger.security = [{"bearer": []}]
  const {configId} = req.params;

  const result = await service.listTransporte(configId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({transportes: result.data})
}

const listManyTransporte = async (req, res) => {
  // #swagger.tags = ['Cd Configs And Modalides de Entrega']
  // #swagger.description = 'Endpoint para listar as modalidades de entrega de uma Configuração.'
  // #swagger.security = [{"bearer": []}]
  const configIds = req.query.ids.split(',');

  const result = await service.listManyTransporte(configIds)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({transportes: result.data})
}

module.exports = {
  create,
  list,
  get,
  update,
  deleteConfig,
  addTransportadora,
  listTransportadoras,
  deleteTransportadora,
  addTransporte,
  listTransporte,
  updateTransporte,
  showTransporte,
  listManyTransporte,
}