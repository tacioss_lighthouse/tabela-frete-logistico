const connection = require("../database/connection");
const service = require("../services/cds")

const create = async (req, res) => {
  // #swagger.tags = ['CDs']
  // #swagger.description = 'Endpoint para criar um CD.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['CD'] = {
        in: 'body',
        description: 'Informações do CD.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddCD" }
    } */
  const cdData = req.body.cd;

  const result = await service.create(cdData)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({cd: result.data})

}
const list = async (req, res) => {

  // #swagger.tags = ['CDs']
  // #swagger.description = 'Endpoint para listar os CD.'
  // #swagger.security = [{"bearer": []}]

  return res.json({cds: await service.list()})

}
const update = async (req, res) => {
  // #swagger.tags = ['CDs']
  // #swagger.description = 'Endpoint para atualizar um CD.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['CD'] = {
        in: 'body',
        description: 'Informações do CD.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateCD" }
    } */
  const {cdId} = req.params;
  const cdData = req.body.cd;

  const result = await service.update(cdId, cdData)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({cd: result.data})
}
const deleteCD = async (req, res) => {
  // #swagger.tags = ['CDs']
  // #swagger.description = 'Endpoint deletar um CD.'
  // #swagger.security = [{"bearer": []}]
  const {cdId} = req.params;

  const result = await service.delete(cdId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send('')
}
const get = async (req, res) => {
  // #swagger.tags = ['CDs']
  // #swagger.description = 'Endpoint obter um CD.'
  // #swagger.security = [{"bearer": []}]
  const {cdId} = req.params;

  const result = await service.get(cdId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(200).send(result.data)
}
const addBrick = async (req, res) => {
  // #swagger.tags = ['Bricks And CDs']
  // #swagger.description = 'Endpoint para adicionar um cd a um Brick.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['CD'] = {
      in: 'body',
      description: 'Informações do CD.',
      required: true,
      type: 'object',
      schema: { $ref: "#/definitions/AddCDToBrick" }
  } */
  const {cdId, brickId} = req.params;
  const {config_id} = req.body;

  const result = await service.addBrick(cdId, brickId, config_id)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({brick: result.data})
}
const listBricks = async (req, res) => {
  // #swagger.tags = ['Bricks And CDs']
  // #swagger.description = 'Endpoint para listar os Bricks de um CD.'
  // #swagger.security = [{"bearer": []}]
  const {cdId} = req.params
  const result = await service.listBricks(cdId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({bricks: result.data})
}
const listCdsByBrick = async (req, res) => {
  // #swagger.tags = ['Bricks And CDs']
  // #swagger.description = 'Endpoint para adicionar um cd a um Brick.'
  // #swagger.security = [{"bearer": []}]
  const {brickId} = req.params
  const result = await service.listCdsByBrick(brickId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({cds: result.data})
}
const deleteBrick = async (req, res) => {
  // #swagger.tags = ['Bricks And CDs']
  // #swagger.description = 'Endpoint para deletar um cd de um Brick.'
  // #swagger.security = [{"bearer": []}]
  const {cdId, brickId} = req.params;
  const result = await service.deleteBrick(cdId, brickId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send()
}

const details = async (req, res) => {
  // #swagger.tags = ['Delivery options']
  // #swagger.description = 'Endpoint para buscar opções de entrega dado um CD'
  const cdId = req.params.cdId
  console.log(cdId)

  const sql = `
    SELECT 
      brick_id,
      cf.id AS cd_config_id,
      b.name AS brick_name,
      cds.description AS cd_description,
      cf.descricao AS cd_config_descricao,
      cf.observacoes AS cd_config_observacoes,
      cf.props AS cd_config_props
    FROM cds
      INNER JOIN bricks_has_cds AS bhc ON bhc.cd_id = cds.id
      INNER JOIN bricks AS b ON b.id = bhc.brick_id
      INNER JOIN cd_config AS cf ON cf.id = bhc.cd_config_id
      WHERE cds.id = ?`

  const [records] = await connection.raw(sql, cdId)
  if (!records?.length) {
    return res.status(404).json({ success: false, message: 'Not Found.' })
  }
  return res.json({ cd_details: records})


}

module.exports = {
  create,
  list,
  update,
  get,
  deleteCD,
  addBrick,
  listBricks,
  listCdsByBrick,
  deleteBrick,
  details,
}