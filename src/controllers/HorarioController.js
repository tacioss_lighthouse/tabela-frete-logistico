const services = require('../services/horarios')

const create = async (req, res) => {
  // #swagger.tags = ['Horarios']
  // #swagger.description = 'Endpoint para criar um Horario.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['BriHorariock'] = {
        in: 'body',
        description: 'Informações do Horario.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddHorario" }
    } */
  const result = await services.create(req.body.horario)
  return res.json({horario: result.data})
}
const list = async (req, res) => {
  // #swagger.tags = ['Horarios']
  // #swagger.description = 'Endpoint para listar todos os Horario.'
  // #swagger.security = [{"bearer": []}]
  const result = await services.list()
  return res.json({horario: result.data})
}
const details = async (req, res) => {
  // #swagger.tags = ['Horarios']
  // #swagger.description = 'Endpoint para obter um Horario.'
  // #swagger.security = [{"bearer": []}]
  const result = await services.details(req.params.id)
  return res.json({horario: result.data})
}

const update = async (req, res) => {
  // #swagger.tags = ['Horarios']
  // #swagger.description = 'Endpoint para atualizar um Horario.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['BriHorariock'] = {
        in: 'body',
        description: 'Informações do Horario.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateHorario" }
    } */
  const result = await services.update(req.params.id, req.body.horario)
  return res.json({horario: result.data})
}

module.exports = {
  create,
  list,
  details,
  update
}