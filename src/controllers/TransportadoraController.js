const repo = require("../repos/TransportadoraRepo");
const service = require("../services/transportadora");

const create = async (req, res) => {
  // #swagger.tags = ['Transportadora']
  // #swagger.description = 'Endpoint para criar uma Transportadora.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['Transportadora'] = {
        in: 'body',
        description: 'Informações do Transportadora.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddTransportadora" }
    } */
  const transportadoraData = req.body.transportadora;

  const result = await service.create(transportadoraData)

  if (!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({ transportadora: result.data })

}

const update = async (req, res) => {
   // #swagger.tags = ['Transportadora']
  // #swagger.description = 'Endpoint para atualizar uma Transportadora.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['Transportadora'] = {
        in: 'body',
        description: 'Informações do Transportadora.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateTransportadora" }
    } */
  const { transportadoraId } = req.params;
  const transportadoraData = req.body.transportadora;

  const result = await service.update(transportadoraId, transportadoraData)

  if (!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({ transportadora: result.data })
}

const deleteTransportadora = async (req, res) => {
  // #swagger.tags = ['Transportadora']
  // #swagger.description = 'Endpoint para deletar uma Transportadora.'
  // #swagger.security = [{"bearer": []}]
  const { transportadoraId } = req.params;

  const result = await service.delete(transportadoraId)

  if (!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send('')
}

const list = async (req, res) => {
  // #swagger.tags = ['Transportadora']
  // #swagger.description = 'Endpoint para listar todas as transportadoras.'
  // #swagger.security = [{"bearer": []}]

  return res.json({ transportadora: await repo.list() })

}

const get = async (req, res) => {
  // #swagger.tags = ['Transportadora']
  // #swagger.description = 'Endpoint para obter uma Transportadora.'
  // #swagger.security = [{"bearer": []}]
  const { transportadoraId } = req.params;

  const result = await service.get(transportadoraId)

  if (!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(200).send(result.data)
}

module.exports = {
  create,
  list,
  update,
  get,
  deleteTransportadora
}