const service = require("../services/auth")

const login = async (request, response) => {
  // #swagger.tags = ['Auth']
  // #swagger.description = 'Endpoint para realizar login.'
  const {user: userCredential, password} = request.body
  const result = await service.login(userCredential, password)

  if (!result.success) {
    if (result.error === 'InvalidCredentials') {
      return response.status(401).json({
        error: result.message
      })
    }
    return response.status(500).json(result)
  }
  return response.status(200).json(result.data)
}

module.exports = {
  login
}