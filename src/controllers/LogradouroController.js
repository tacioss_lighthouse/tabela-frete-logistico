
const repo = require("../repos/LogradouroRepo");
const service = require("../services/logradouros");


const create = async (req, res) => {
  // #swagger.tags = ['Logradouros']
  // #swagger.description = 'Endpoint para criar um Logradouro.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['Logradouro'] = {
        in: 'body',
        description: 'Informações do Logradouro.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/AddLogradouro" }
    } */
  const logradouroData = req.body.logradouro;

  const result = await service.create(logradouroData)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({logradouro: result.data})

}

const list = async (req, res) => {
  // #swagger.tags = ['Logradouros']
  // #swagger.description = 'Endpoint para listar todos os Logradouros.'
  // #swagger.security = [{"bearer": []}]

  return res.json({logradouros: await repo.list()})

}

const update = async (req, res) => {
  // #swagger.tags = ['Logradouros']
  // #swagger.description = 'Endpoint para atualizar um Logradouro.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['Logradouro'] = {
        in: 'body',
        description: 'Informações do Logradouro.',
        required: true,
        type: 'object',
        schema: { $ref: "#/definitions/UpdateLogradouro" }
    } */
  const {logradouroId} = req.params;
  const logradouroData = req.body.logradouro;

  const result = await service.update(logradouroId, logradouroData)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(201).json({logradouro: result.data})
}

const deleteLogradouro = async (req, res) => {
  // #swagger.tags = ['Logradouros']
  // #swagger.description = 'Endpoint para deletar um Logradouro.'
  // #swagger.security = [{"bearer": []}]
  const {logradouroId} = req.params;

  const result = await service.delete(logradouroId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(204).send('')
}

const get = async (req, res) => {
  // #swagger.tags = ['Logradouros']
  // #swagger.description = 'Endpoint para obter um Logradouro.'
  // #swagger.security = [{"bearer": []}]
  const {logradouroId} = req.params;

  const result = await service.get(logradouroId)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(200).send(result.data)
}

const search = async (req, res) => {
  // #swagger.tags = ['Logradouros']
  // #swagger.description = 'Endpoint para pesquisar logradouros.'
  // #swagger.security = [{"bearer": []}]
  /* #swagger.parameters['cep'] = { in: 'query' } */
  /* #swagger.parameters['logradouro'] = { in: 'query' } */
  /* #swagger.parameters['bairro'] = { in: 'query' } */
  /* #swagger.parameters['cidade'] = { in: 'query' } */
  /* #swagger.parameters['uf'] = { in: 'query' } */
  const queryObject = req.query;

  const result = await service.search(queryObject)

  if(!result.success) {
    return res.status(400).json({
      error: result.message
    })
  }
  return res.status(200).json({logradouros: result.data})
}

module.exports = {
  create,
  list,
  update,
  get,
  deleteLogradouro,
  search,
}