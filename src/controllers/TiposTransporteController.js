const services = require('../services/tiposDeTransporte.js')

const list = async(req, res) => {
  // #swagger.tags = ['Tipos de Transporte']
  // #swagger.description = 'Endpoint para buscar tipos de entrega'
  return res.json({tipos_entrega: await services.list()})
}


module.exports = {
  list,
}