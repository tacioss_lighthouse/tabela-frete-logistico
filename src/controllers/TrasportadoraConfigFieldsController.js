const list = (req, res) => {
  // #swagger.tags = ['CD Config Props']
  // #swagger.description = 'Endpoint para listar os campos de propriedade das configurações.'
  return res.json({
    prop_fields: [
      {
        label: 'Tempo Máximo de Entrega',
        id: 1,
        field: 'tempo_entrega_max',
        required: true,
      },
      {
        label: 'Tempo Mínimo de Entrega',
        id: 2,
        field: 'tempo_entrega_min',
        required: true,
      },
    ]
  })
}
module.exports = {
  list,
}