const { errorResult, successResult } = require("../utils/result");
const repo = require('../repos/CDsRepo')
const brickRepo = require('../repos/BrickRepo')

const create = async (cdData) => {
  const {description, id} = cdData;

  const result = await repo.create({description, id})
  if (!result.success) {
    return successResult
  }

  return successResult(result.data)
}
const list = async () => {
  return repo.list()
}
const update = async (cdId, cdData) => {

  const cdInDataBase = await repo.getById(cdId)

  if (!cdInDataBase) {
    return errorResult('Not Found.')
  }

  const cdRow = await repo.update(cdId, cdData)

  return successResult(cdRow)
}
const deleteCD = async (cdId) => {

  const cdInDatabase = await repo.getById(cdId)

  if (!cdInDatabase) {
    return errorResult('Not Found.')
  }

  await repo.delete(cdId)
  
  return successResult('OK')
}
const get = async (cdId) => {

  const cdInDatabase = await repo.getById(cdId)

  if (!cdInDatabase) {
    return errorResult('Not Found.')
  }

  return successResult(cdInDatabase)
}
const addBrick = async (cdId, brickId, cd_config_id) => {
  const cdInDataBase = await repo.getById(cdId)

  if (!cdInDataBase) {
    return errorResult('CD Not Found.')
  }

  const brickInDatabase = await brickRepo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Brick Not Found.')
  }

  const cds = await brickRepo.getCds(brickInDatabase)

  if (cds.find(c => c.id === cdInDataBase.id)) {
    return errorResult('Este brick já está associado a este CD.')
  }

  await repo.addBrick(cdInDataBase, brickInDatabase, cd_config_id)

  return successResult(brickInDatabase)
}
const listBricks = async (cdId) => {
  const cdInDataBase = await repo.getById(cdId)

  if (!cdInDataBase) {
    return errorResult('CD Not Found.')
  }
  const bricks = await repo.getBricks(cdInDataBase)
  return successResult(bricks)
}
const listCdsByBrick = async (brickId) => {
  const brick = await brickRepo.getById(brickId)

  if (!brick) {
    return errorResult('CD Not Found.')
  }
  const cds = await brickRepo.getCds(brick)
  return successResult(cds)
}
const deleteBrick = async (cdId, brickId) => {
  const cdInDatabase = await repo.getById(cdId)

  if (!cdInDatabase) {
    return errorResult('CD Not Found.')
  }

  const brickInDatabase = await brickRepo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Brick Not Found.')
  }

  await repo.deleteBrick(cdInDatabase, brickInDatabase)

  return successResult(brickInDatabase)
}


module.exports = {
  create,
  list,
  update,
  delete: deleteCD,
  get,
  addBrick,
  listBricks,
  listCdsByBrick,
  deleteBrick,
}