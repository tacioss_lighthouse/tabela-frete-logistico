const { errorResult, successResult } = require("../utils/result");
const repo = require('../repos/UsersRepo');

const create = async (userData) => {
  const {username, first_name, last_name, email, password, password_confirmation} = userData

  if (password !== password_confirmation) {
    return errorResult('Password confirmation don\'t match', {error: 'BadRequest'})
  }

  if (!(validatePasswordSecurity(password))) {
    return errorResult(
      'Invalid password field. Password need to have at least: '
      + 'a digit, a upper case letter, and a lower case letter',
      {error: 'BadRequest'})
  }

  const userAlreadyInDatabase = await repo.getUserWithCredentialsConflict({
    username, email
  })
  
  if (userAlreadyInDatabase) {
    return errorResult('There is already an user with this username or e-mail.', {error: 'Forbidden'})
  }

  const userCreated = await repo.create({
    username, first_name, last_name, email, password
  })

  return successResult(userCreated)

}

const update = async (userData, loggedUserId) => {
  const { id, username, email } = userData

  if (loggedUserId !== id) {
    return errorResult('You can only update your user.', {error: 'Forbidden'})
  }

  const user = await repo.getById(id)

  if (!user) {
    return errorResult(`User with id #${id} not found.`, {error: 'NotFound'})
  }
  const userAlreadyInDatabase = await repo.getUserWithCredentialsConflict({
    username, email
  })
  if (userAlreadyInDatabase && userAlreadyInDatabase.id !== user.id){
    return errorResult('There is already an user with this username or e-mail.', {error: 'Forbidden'})

  }

  await repo.update(id, userData)
  
  const userUpdated = await repo.getById(id)

  return successResult(userUpdated)
}


const validatePasswordSecurity = (password) => {
  const regexValidate = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d]{8,}$/
  return regexValidate.test(password)
}


module.exports = {
  create,
  update,
}