const { errorResult, successResult } = require("../utils/result");
const repo = require("../repos/TransportadoraRepo");

const configRepo = require('../repos/CDConfigsRepo')

const create = async (transportadoraData) => {
  const {tipo_pessoa, cnpj, inscricao_estadual, razao_social, nome_fantasia, cep, rua, numero, complemento,
    bairro, cidade, estado, email, telefone, observacoes} = transportadoraData;

  const transportadoraAlreadyInDatabase = await repo.getByCnpj(cnpj)

  if (transportadoraAlreadyInDatabase) {
    return errorResult('Esta transportadora ja foi cadastrado.')
  }

  const transportadoraRow = await repo.create({tipo_pessoa, cnpj, inscricao_estadual, razao_social, nome_fantasia, cep, rua, numero, complemento,
    bairro, cidade, estado, email, telefone, observacoes})
  if(!transportadoraRow) {
    return errorResult('Erro ao criar transportadora')
  }

  return successResult(transportadoraRow)
}

const list = async () => {
  return repo.list()
}

const update = async (transportadoraId, transportadoraData) => {
  const {cnpj} = transportadoraData

  const transportadoraInDatabase = await repo.getById(transportadoraId)

  if (!transportadoraInDatabase) {
    return errorResult('Not Found.')
  }

  const updateObject = {
    ...transportadoraData
  }
  if (cnpj) {
    const transportadoraAlreadyInDatabase = await repo.getByCnpj(updateObject.cnpj)
    if (transportadoraAlreadyInDatabase && transportadoraAlreadyInDatabase.id !== transportadoraInDatabase.id) {
      return errorResult('Esta transportadora ja foi cadastrado.')
    }
  }

  const transportadoraRow = await repo.update(transportadoraId, updateObject)

  return successResult(transportadoraRow)
}

const deleteTransportadora = async (transportadoraId) => {

  const transportadoraInDatabase = await repo.getById(transportadoraId)

  if (!transportadoraInDatabase) {
    return errorResult('Not Found.')
  }

  await repo.delete(transportadoraId)
  
  return successResult('OK')
}

const get = async (transportadoraId) => {

  const transportadoraInDatabase = await repo.getById(transportadoraId)

  if (!transportadoraInDatabase) {
    return errorResult('Not Found.')
  }

  return successResult(transportadoraInDatabase)
}


module.exports = {
  create,
  list,
  update,
  get,
  delete: deleteTransportadora
}