const service = require('../services/horarios')
const HorarioRepo = require('../repos/HorarioRepo')
const { successResult } = require('../utils/result')

const create = async (horarioData) => {

  const horario = await HorarioRepo.create(horarioData)

  return successResult(horario)
}
const list = async () => {
  
  const horarios = await HorarioRepo.list()
  
  return successResult(horarios)
}
const details = async (id) => {
  
  const horarios = await HorarioRepo.details(id)
  
  return successResult(horarios)
}
const update = async (id, horarioData) => {

  const horario = await HorarioRepo.update(id, horarioData)

  return successResult(horario)
}


module.exports = {
  create,
  list,
  details,
  update,
}