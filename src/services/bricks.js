const { errorResult, successResult } = require("../utils/result");
const repo = require("../repos/BrickRepo");
const logradouroRepo = require("../repos/LogradouroRepo");
const transportadoraRepo = require("../repos/TransportadoraRepo");
const transportadoraConfigRepo = require('../repos/CDConfigsRepo')


const create = async (brickData) => {
  const {name, description} = brickData;

  const brickAlreadyInDatabase = await repo.getByName(name)

  if (brickAlreadyInDatabase) {
    return errorResult('Este Brick ja foi cadastrado.')
  }

  const brickRow = await repo.create({name, description})

  return successResult(brickRow)
}
const list = async () => {
  return repo.list()
}
const update = async (brickId, brickData) => {
  const {name} = brickData

  const brickInDatabase = await repo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Not Found.')
  }

  if (name) {
    const brickAlreadyInDatabase = await repo.getByName(name)
    if (brickAlreadyInDatabase && brickAlreadyInDatabase.id !== brickInDatabase.id) {
      return errorResult('Este Brick ja foi cadastrado.')
    }
  }

  const brickRow = await repo.update(brickId, brickData)

  return successResult(brickRow)
}
const deleteBrick = async (brickId) => {

  const brickInDatabase = await repo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Not Found.')
  }

  await repo.delete(brickId)
  
  return successResult('OK')
}
const get = async (brickId) => {

  const brickInDatabase = await repo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Not Found.')
  }
  
  return successResult(brickInDatabase)
}
const addLogradouro = async (brickId, logradouroId) => {
  const brickInDatabase = await repo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Brick Not Found.')
  }

  const logradouroInDatabase = await logradouroRepo.getById(logradouroId)

  if (!logradouroInDatabase) {
    return errorResult('Logradouro Not Found.')
  }

  const logradouros = await repo.getLogradouros(brickInDatabase)

  if (logradouros.find(l => l.id === logradouroInDatabase.id)) {
    return errorResult('Este logradouro já está associado a este Brick.')
  }

  await repo.addLogradouro(brickInDatabase, logradouroInDatabase)

  return successResult(logradouroInDatabase)
}
const listLogradouros = async (brickId) => {
  const brickInDatabase = await repo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Brick Not Found.')
  }
  const logradouros = await repo.getLogradouros(brickInDatabase)
  return successResult(logradouros)
}
const deleteLogradouro = async (brickId, logradouroId) => {
  const brickInDatabase = await repo.getById(brickId)

  if (!brickInDatabase) {
    return errorResult('Brick Not Found.')
  }

  const logradouroInDatabase = await logradouroRepo.getById(logradouroId)

  if (!logradouroInDatabase) {
    return errorResult('Logradouro Not Found.')
  }

  await repo.deleteLogradouro(brickInDatabase, logradouroInDatabase)

  return successResult(logradouroInDatabase)
}

module.exports = {
  create,
  list,
  update,
  get,
  delete: deleteBrick,
  addLogradouro,
  listLogradouros,
  deleteLogradouro,
}