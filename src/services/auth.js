const bcrypt = require("bcrypt")
const jsonwebtoken = require("jsonwebtoken");
const { errorResult, successResult } = require("../utils/result");

const repo = require('../repos/UsersRepo');
const { JWT_SECRET } = require("../config");


const login = async (userCredential, password) => {

  const user = await repo.getUserByCredentials(userCredential)
  
  if (!user || !bcrypt.compareSync(password, user.password_hash)) {
    return errorResult('Invalid username or password.', {error: 'InvalidCredentials'})
  }

  const token = jsonwebtoken.sign({id: user.id}, JWT_SECRET, {expiresIn: '3h'})

  return successResult({
    token,
    user: {
      id: user.id,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
    }
  })
}

module.exports = {
  login
}