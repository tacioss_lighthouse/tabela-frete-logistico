const { errorResult, successResult } = require("../utils/result");
const repo = require("../repos/LogradouroRepo");

const create = async (logradouroData) => {
  const {cep, logradouro, bairro, cidade, uf} = logradouroData;

  const logradouroAlreadyInDatabase = await repo.getByCEP(cep)

  if (logradouroAlreadyInDatabase) {
    return errorResult('Este logradouro ja foi cadastrado.')
  }

  const logradouroRow = await repo.create({cep, logradouro, bairro, cidade, uf})

  return successResult(logradouroRow)
}

const list = async () => {
  return repo.list()
}

const update = async (logradouroId, logradouroData) => {
  const {cep} = logradouroData

  const logradouroInDatabase = await repo.getById(logradouroId)

  if (!logradouroInDatabase) {
    return errorResult('Not Found.')
  }

  const updateObject = {
    ...logradouroData
  }
  if (cep) {
    updateObject.cep = cep.replaceAll(/[^0-9]/g, '')
    const logradouroAlreadyInDatabase = await repo.getByCEP(updateObject.cep)
    if (logradouroAlreadyInDatabase && logradouroAlreadyInDatabase.id !== logradouroInDatabase.id) {
      return errorResult('Este logradouro ja foi cadastrado.')
    }
  }

  const logradouroRow = await repo.update(logradouroId, updateObject)

  return successResult(logradouroRow)
}

const deleteLogradouro = async (logradouroId) => {

  const logradouroInDatabase = await repo.getById(logradouroId)

  if (!logradouroInDatabase) {
    return errorResult('Not Found.')
  }

  await repo.delete(logradouroId)
  
  return successResult('OK')
}

const get = async (logradouroId) => {

  const logradouroInDatabase = await repo.getById(logradouroId)

  if (!logradouroInDatabase) {
    return errorResult('Not Found.')
  }

  return successResult(logradouroInDatabase)
}

const search = async (searchObject) => {

  const result = await repo.search(searchObject)

  if (!result) {
    return errorResult('Not Found.')
  }

  return successResult(result)
}


module.exports = {
  create,
  list,
  update,
  get,
  delete: deleteLogradouro,
  search,
}