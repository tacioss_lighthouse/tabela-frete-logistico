const repo = require('../repos/CDConfigsRepo')
const transportadoraRepo = require('../repos/TransportadoraRepo')
const cdRepo = require('../repos/CDsRepo')
const brickRepo = require('../repos/BrickRepo')
const { successResult, errorResult } = require('../utils/result')

const create = async (configData) => {
  // TODO: validar se existe
  const config = await repo.create(configData)
  if (!config) {
    return errorResult('Not created')
  }

  return successResult(config)
}
const list = async () => {
  // TODO: validar se existe
  const configs = await repo.listByBrickAndCD()

  return successResult(configs)
}

const get = async (configId) => {
  const config = await repo.getById(configId)
  if (!config) {
    return errorResult('NotFound')
  }
  return successResult(config)
}
const update = async (configId, configData) => {
  const configInDatabase = await repo.getById(configId)
  if (!configInDatabase) {
    return errorResult('Not Found')
  }
  const config = await repo.update(configId, configData)
  return successResult(config)
}
const deleteConfig = async (configId) => {

  const configInDatabase = await repo.getById(configId)
  if (!configInDatabase) {
    return errorResult('Not Found')
  }
  await repo.delete(configId)
  return successResult('Ok')
}


const addTransportadora = async (configId, transportadoraId) => {
  const configInDatabase = await repo.getById(configId)

  if (!configInDatabase) {
    return errorResult('Config Not Found.')
  }

  const transportadoraInDatabase = await transportadoraRepo.getById(transportadoraId)

  if (!transportadoraInDatabase) {
    return errorResult('Transportadora Not Found.')
  }

  const result = await repo.addTransportadora(configInDatabase, transportadoraInDatabase)

  if(result) {
    return errorResult(result.message)
  }

  return successResult(transportadoraInDatabase)
}

const listTransportadoras = async (configId) => {
  const config = await repo.getById(configId)

  if (!config) {
    return errorResult('Config Not Found.')
  }
  const transportadoras = await repo.getTransportadoras(config)
  return successResult(transportadoras)
}

const deleteTransportadora = async (configId, transportadoraId) => {
  const config = await repo.getById(configId)

  if (!config) {
    return errorResult('Config Not Found.')
  }

  const transportadora = await transportadoraRepo.getById(transportadoraId)

  if (!transportadora) {
    return errorResult('Transportadora Not Found.')
  }

  await repo.deleteTransportadora(config, transportadora)
  return successResult(transportadora)
}

const addTransporte = async (configId, valor, tipo_transporte_id, tempo_entrega, horario_id) => {
  return await repo.addTransporte(configId, valor, tipo_transporte_id, tempo_entrega, horario_id)
}
const updateTransporte = async (configId, valor, tipo_transporte_id, tempo_entrega, horario_id) => {
  return successResult(await repo.updateTransporte(configId, valor, tipo_transporte_id, tempo_entrega, horario_id))
}
const showTransporte = async (configId, tipo_transporte_id) => {
  return await repo.showTransporte(configId, tipo_transporte_id)
}
const listTransporte = async (configId, valor, tipo_transporte_id) => {
  return successResult(await repo.listTransporte(configId, valor, tipo_transporte_id))
}
const listManyTransporte = async (configIds) => {

  return successResult(await repo.listManyTransporte(configIds))
}


module.exports = {
  create,
  list,
  get,
  update,
  delete: deleteConfig,
  addTransportadora,
  listTransportadoras,
  deleteTransportadora,
  addTransporte,
  updateTransporte,
  listTransporte,
  showTransporte,
  listManyTransporte,
}