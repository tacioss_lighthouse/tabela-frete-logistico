const repo = require('../repos/TiposDeTransporte')

const list = async () => {
  return await repo.list()
}


module.exports = {
  list,
}