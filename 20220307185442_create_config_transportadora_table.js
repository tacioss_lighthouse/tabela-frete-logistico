
exports.up = async function(knex) {
  await knex.schema.createTable('config_tranportadora', table => {
    table.increments('id')
    table.string('descricao', 80)
    table.text('observacoes')
    table.jsonb('props').notNullable()
    table.integer('tipo_transporte_id').unsigned().notNullable()
      .references('id').inTable('tipos_tranporte')
  })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('config_tranportadora')
};
