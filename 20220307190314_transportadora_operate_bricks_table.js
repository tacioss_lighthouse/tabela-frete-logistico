
exports.up = async function(knex) {
  await knex.schema.createTable('transportadoras_operates_bricks', table => {
    table.integer('transportadora_id').unsigned().notNullable()
      .references('id').inTable('transportadoras')
    table.integer('brick_id').unsigned().notNullable()
      .references('id').inTable('bricks')
    table.integer('config_transportadora_id').unsigned().notNullable()
      .references('id').inTable('config_tranportadora')
    table.primary(['brick_id', 'transportadora_id', 'config_transportadora_id'])
    })
};

exports.down = async function(knex) {
  await knex.schema.dropTable('transportadoras_operates_bricks')
};
