// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      database: 'frete',
      user:     'user',
      password: '123456'
    },
    migrations: {
      directory: __dirname + '/src/database/migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: __dirname + '/src/database/seeds',
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'mysql2',
    connection: process.env.JAWSDB_URL,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: __dirname + '/src/database/migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: __dirname + '/src/database/seeds',
    }
  }
  // production: {
  //   client: 'mysql2',
  //   connection: {
  //     database: 'frete',
  //     user:     'user',
  //     password: '123456'
  //   },
  //   migrations: {
  //     directory: __dirname + '/src/database/migrations',
  //     tableName: 'knex_migrations'
  //   },
  //   seeds: {
  //     directory: __dirname + '/src/database/seeds',
  //   }
  // },

};
